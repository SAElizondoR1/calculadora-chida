#!/bin/bash
if command -v cmake >/dev/null 2>&1 ; then
	echo "Se encontró CMake."
else
	echo "No se encontró CMake. Instálelo y vuelva a intentar."
	exit
fi
if ldconfig -p | grep gmp ; then
  echo "Se encontró la librería GMP."
else
  echo "No se encontró la librería GMP. Instálela y vuelva a intentar."
  exit
fi
if ldconfig -p | grep gmpxx ; then
  echo "Se encontró la librería GMPXX."
else
  echo "No se encontró la librería GMPXX. Instálela y vuelva a intentar."
  exit
fi
if ldconfig -p | grep mpfr ; then
  echo "Se encontró la librería MPFR."
else
  echo "No se encontró la librería MPFR. Instálela y vuelva a intentar."
  exit
fi
if  test -f /usr/include/mpreal.h || test -f /usr/local/include/mpreal.h ; then
  echo "Se encontró la librería mpfrc++"
else
  echo "No se encontró la librería mpfrc++.Instálela y vuelva a intentar."
  exit
fi
mkdir construir
cd construir || exit
cmake ..
cmake --build .
sudo cmake --build . --target install
