//
// Created by sergio on 04/02/21.
//

#ifndef CALCULADORA_CHIDA_ANALIZADOR_LEXICO_H
#define CALCULADORA_CHIDA_ANALIZADOR_LEXICO_H

#include <string>
#include <memory>

// Componentes válidos
enum class TipoDeLexema {
    ENTERO,
    REAL,
    MAS,
    MENOS,
    POR,
    ENTRE,
    POTENCIA,
    PARENTESIS_IZQ,
    PARENTESIS_DER,
    FIN_DEL_TEXTO,
    DESCONOCIDO
};

class Lexema {
public:
    Lexema(std::string valor, TipoDeLexema tipo);
    [[nodiscard]] bool es_valido() const;  // Lexema válido.
    std::string obtener_valor();
    TipoDeLexema obtener_tipo();

private:
    std::string valor;
    TipoDeLexema tipo;
};

class AnalizadorLexico {
public:
    explicit AnalizadorLexico(const std::string &texto);    // Constructor
    std::unique_ptr<Lexema> obtener_siguiente_lexema();  // Devuelve el siguiente lexema encontrado.

private:
    std::string texto;  // Texto de entrada
    char caracter_actual;
    int indice;         // Índice del caracter actual

    [[nodiscard]] bool llego_al_final();      // ¿Se han leído ya todos los caracteres del texto?
    void asignar_caracter_actual(); // Asigna el valor que corresponda al carácter actual.
    void avanzar_posicion();    // Incrementa el índice en 1.
    void saltar_espacios(); // Brinca espacios hasta encontrar otro carácter.
    std::unique_ptr<Lexema> obtener_siguiente_numero();
    // Devuelve el siguiente número (o desconocido si hay error).
};

#endif //CALCULADORA_CHIDA_ANALIZADOR_LEXICO_H

