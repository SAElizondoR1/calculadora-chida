//
// Created by sergio on 07/02/21.
//

#include <utility>

#include "analizador_lexico.h"
Lexema::Lexema(std::string valor, TipoDeLexema tipo) {
    this->valor = std::move(valor);
    this->tipo = tipo;
}

bool Lexema::es_valido() const {
    return this->tipo != TipoDeLexema::DESCONOCIDO;
}

std::string Lexema::obtener_valor() {
    return this->valor;
}

TipoDeLexema Lexema::obtener_tipo() {
    return this->tipo;
}

AnalizadorLexico::AnalizadorLexico(const std::string &texto) {
    this->texto = texto;
    caracter_actual = '\0';
    indice = 0;
    asignar_caracter_actual();
}

std::unique_ptr<Lexema> AnalizadorLexico::obtener_siguiente_lexema() {
    saltar_espacios();
    // Si el carácter actual es nulo.
    if (!caracter_actual) {
        avanzar_posicion();
        return std::make_unique<Lexema>("", TipoDeLexema::FIN_DEL_TEXTO);
    }
    if (std::isdigit(caracter_actual) || caracter_actual == '.')
        return obtener_siguiente_numero();
    if (caracter_actual == '+') {
        avanzar_posicion();
        return std::make_unique<Lexema>("+", TipoDeLexema::MAS);
    }
    if (caracter_actual == '-') {
        avanzar_posicion();
        return std::make_unique<Lexema>("-", TipoDeLexema::MENOS);
    }
    if (caracter_actual == '*') {
        avanzar_posicion();
        return std::make_unique<Lexema>("*", TipoDeLexema::POR);
    }
    if (caracter_actual == '/') {
        avanzar_posicion();
        return std::make_unique<Lexema>("/", TipoDeLexema::ENTRE);
    }
    if (caracter_actual == '^') {
        avanzar_posicion();
        return std::make_unique<Lexema>("^", TipoDeLexema::POTENCIA);
    }
    if (caracter_actual == '(') {
        avanzar_posicion();
        return std::make_unique<Lexema>("(", TipoDeLexema::PARENTESIS_IZQ);
    }
    if (caracter_actual == ')') {
        avanzar_posicion();
        return std::make_unique<Lexema>(")", TipoDeLexema::PARENTESIS_DER);
    }

    // Si el lexema no coincide con ningún símbolo.
    return std::make_unique<Lexema>(std::string(1, caracter_actual), TipoDeLexema::DESCONOCIDO);
}

bool AnalizadorLexico::llego_al_final() {
    return indice >= texto.length();
}

void AnalizadorLexico::asignar_caracter_actual() {
    if (llego_al_final())
        caracter_actual = '\0';
    else
        caracter_actual = texto[indice];
}

void AnalizadorLexico::avanzar_posicion() {
    indice++;
    asignar_caracter_actual();
}

void AnalizadorLexico::saltar_espacios() {
    while (std::isspace(caracter_actual))
        avanzar_posicion();
}

std::unique_ptr<Lexema> AnalizadorLexico::obtener_siguiente_numero() {
    std::string numero;
    TipoDeLexema tipoDeLexema  = TipoDeLexema::ENTERO;

    if (caracter_actual == '.') {   // se consume .numero+
        numero += caracter_actual;
        avanzar_posicion();
        tipoDeLexema = TipoDeLexema::REAL;    // si encuentra un punto, entonces el número es real.

        if (std::isdigit(caracter_actual)) {    // se debe consumir al menos un número ("." solo no es válido).
            numero += caracter_actual;
            avanzar_posicion();
        } else {
            return std::make_unique<Lexema>(numero, TipoDeLexema::DESCONOCIDO);
        }
    } else if (std::isdigit(caracter_actual)) { // en este caso se consume numero+.?numero*
        while (std::isdigit(caracter_actual)) {
            numero += caracter_actual;
            avanzar_posicion();
        }
        if (caracter_actual == '.') {
            numero += caracter_actual;
            avanzar_posicion();
            tipoDeLexema = TipoDeLexema::REAL;
        }
    }

    while (std::isdigit(caracter_actual)) {
        numero += caracter_actual;
        avanzar_posicion();
    }

    return std::make_unique<Lexema>(numero, tipoDeLexema);
}
