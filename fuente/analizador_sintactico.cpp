//
// Created by sergio on 07/02/21.
//

#include <utility>

#include "analizador_sintactico.h"

AnalizadorSintactico::AnalizadorSintactico(const std::string &texto) {
    analizador_lexico = std::make_unique<AnalizadorLexico>(texto);
    lexema_actual = nullptr;
    raiz = nullptr;
    mas_ala_derecha = nullptr;
    error = TipoDeError::NINGUNO;
}

std::unique_ptr<Nodo> AnalizadorSintactico::analizar() {
    if(!crear_arbol()) {    // Si no se pudo crear el árbol.
        if (!lexema_actual->es_valido()) {   // Si el lexema no es válido.
            error = TipoDeError::ERROR_LEXICO;
            return std::make_unique<Nodo>(std::move(lexema_actual), Precedencia::DESCONOCIDO);
        }
        else {
            error = TipoDeError::ERROR_SINTACTICO;
            return std::unique_ptr<Nodo>();
        }
    }
    return std::move(raiz);
}

TipoDeError AnalizadorSintactico::obtener_error() {
    return this->error;
}

bool AnalizadorSintactico::crear_arbol() {
    return consumir_expresion(TipoDeLexema::FIN_DEL_TEXTO); // consumir expresión hasta el fin del texto.
}

bool AnalizadorSintactico::consumir_expresion(TipoDeLexema hastaDonde) {
    if (!consumir_argumento())  // si no se pudo consumir un argumento
        return false;
    if (!consumir_operaciones(hastaDonde)) // si no se pudieron consumir más operaciones (hasta cierto punto)
        return false;
    return true;
}

bool AnalizadorSintactico::consumir_argumento() {
    lexema_actual = analizador_lexico->obtener_siguiente_lexema();
    std::unique_ptr<Nodo> nodo;

    switch (lexema_actual->obtener_tipo()) {
        case TipoDeLexema::PARENTESIS_IZQ: {
            nodo = std::make_unique<NodoParentesis>(std::move(lexema_actual));  // nodo con precedencia de número
            insertar_nodo(std::move(nodo), false);
            Nodo *parentesis = mas_ala_derecha; // el nodo insertado
            parentesis->precedencia = Precedencia::MAXIMA;  // se cambia a precedencia máxima
            // para que la expresión dentro del paréntesis como un sub-árbol
            if (!consumir_expresion(TipoDeLexema::PARENTESIS_DER)) // se consume una expresión.
                return false;
            parentesis->precedencia = Precedencia::NUMERO;  // se regresa a la precedencia original.
            return true;
        }
        case TipoDeLexema::ENTERO:
            nodo = std::make_unique<NodoEntero>(std::move(lexema_actual));
            insertar_nodo(std::move(nodo), false);
            return true;
        case TipoDeLexema::REAL:
            nodo = std::make_unique<NodoReal>(std::move(lexema_actual));
            insertar_nodo(std::move(nodo), false);
            return true;
        case TipoDeLexema::MENOS: // - unario
            nodo = std::make_unique<NodoMenosUnario>(std::move(lexema_actual));
            insertar_nodo(std::move(nodo), false); // insertar -, después consumir argumento.
        case TipoDeLexema::MAS:   // + unario, no tiene efecto
            if (!consumir_argumento())
                return false;
            return true;
        default:
            return false;
    }
}

bool AnalizadorSintactico::consumir_operaciones(TipoDeLexema hastaDonde) {
    std::unique_ptr<Nodo> nodo;
    lexema_actual = analizador_lexico->obtener_siguiente_lexema();

    if (lexema_actual->obtener_tipo() == hastaDonde)
        return true;

    switch (lexema_actual->obtener_tipo()) {
        case TipoDeLexema::MAS:
            nodo = std::make_unique<NodoSuma>(std::move(lexema_actual));
            insertar_nodo(std::move(nodo), true);
            break;
        case TipoDeLexema::MENOS:
            nodo = std::make_unique<NodoResta>(std::move(lexema_actual));
            insertar_nodo(std::move(nodo), true);
            break;
        case TipoDeLexema::POR:
            nodo = std::make_unique<NodoMultiplicacion>(std::move(lexema_actual));
            insertar_nodo(std::move(nodo), true);
            break;
        case TipoDeLexema::ENTRE:
            nodo = std::make_unique<NodoDivision>(std::move(lexema_actual));
            insertar_nodo(std::move(nodo), true);
            break;
        case TipoDeLexema::POTENCIA:
            nodo = std::make_unique<NodoPotencia>(std::move(lexema_actual));
            insertar_nodo(std::move(nodo), true);
            break;
        default:
            return false;
    }

    if (!consumir_argumento())
        return false;
    if (!consumir_operaciones(hastaDonde))
        return false;

    return true;
}

void AnalizadorSintactico::insertar_nodo(std::unique_ptr<Nodo> nodo, bool esBinario) {
    if (raiz == nullptr) {
        raiz = std::move(nodo);
        mas_ala_derecha = raiz.get();
        return;
    }

    Nodo* tmp = mas_ala_derecha;
    // Mientras tmp no sea nulo y tenga mayor o igual precedencia que el nodo, hacer tmp = tmp->padre
    while (tmp != nullptr && tmp->comparar_precedencia(nodo.get()))
        tmp = tmp->padre;

    if (esBinario) {    // Proceso para insertar nodo binario
        if (tmp == nullptr) { // si tuvo mayor o igual precedencia que el nodo raiz
            raiz->padre = nodo.get();
            nodo->hijo_izq = std::move(raiz);
            mas_ala_derecha = nodo.get();
            raiz = std::move(nodo);

        } else {
            nodo->hijo_izq = std::move(tmp->hijo_der);
            if (nodo->hijo_izq)
                nodo->hijo_izq->padre = nodo.get();

            nodo->padre = tmp;
            mas_ala_derecha = nodo.get();
            tmp->hijo_der = std::move(nodo);
        }
    } else {    // Proceso para insertar nodo unario o número
        if (tmp == nullptr) { // si tuvo mayor o igual precedencia que el nodo raiz
            raiz->padre = nodo.get();
            nodo->hijo_der = std::move(raiz);
            mas_ala_derecha = nodo.get();
            raiz = std::move(nodo);
        } else {
            nodo->hijo_der = std::move(tmp->hijo_der);
            if (nodo->hijo_der)
                nodo->hijo_der->padre = nodo.get();

            nodo->padre = tmp;
            mas_ala_derecha = nodo.get();
            tmp->hijo_der = std::move(nodo);
        }
        while (mas_ala_derecha->hijo_der)
            mas_ala_derecha = mas_ala_derecha->hijo_der.get();
    }
}
