//
// Created by sergio on 29/03/21.
//

#include "nodos.h"

Nodo::Nodo(std::unique_ptr<Lexema> lexema, Precedencia precedencia,
           Asociatividad asociatividad) {
    this->lexema = std::move(lexema);
    padre = nullptr;
    hijo_izq = nullptr;
    hijo_der = nullptr;
    this->precedencia = precedencia;
    this->asociatividad = asociatividad;
    error = TipoDeError::NINGUNO;
}

bool Nodo::comparar_precedencia(Nodo* nodo) const {
    if (this->precedencia > nodo->precedencia)  // verdadero si el nodo es mayor
        return true;
    else if (this->precedencia < nodo->precedencia) // falso si es menor
        return false;
    return nodo->asociatividad != Asociatividad::DERECHA; // si tienen igual precedencia, revisar asociatividad.
}

mpfr::mpreal Nodo::convertir_a_real(const mpz_class *ap_numero) {
    return mpfr::mpreal(ap_numero->get_mpz_t());
}

mpfr::mpreal Nodo::convertir_a_real(const numero_variante* ap_numero) {
    if (std::holds_alternative<mpz_class>(*ap_numero))
        return convertir_a_real(&std::get<mpz_class>(*ap_numero));
    return std::get<mpfr::mpreal>(*ap_numero);
}

Lexema* Nodo::obtener_lexema() {
    return lexema.get();
}

TipoDeError Nodo::obtener_error() {
    return error;
}

NodoNumero::NodoNumero(std::unique_ptr<Lexema> lexema) : Nodo(std::move(lexema), Precedencia::NUMERO) {

}

NodoEntero::NodoEntero(std::unique_ptr<Lexema> lexema) : NodoNumero(std::move(lexema)) {

}

std::unique_ptr<numero_variante> NodoEntero::resolver() {
    mpz_class entero = mpz_class(this->lexema->obtener_valor());
    return std::make_unique<numero_variante>(entero);
}

NodoReal::NodoReal(std::unique_ptr<Lexema> lexema) : NodoNumero(std::move(lexema)) {

}

std::unique_ptr<numero_variante> NodoReal::resolver() {
    mpfr::mpreal real = mpfr::mpreal(this->lexema->obtener_valor());
    return std::make_unique<numero_variante>(real);
}

NodoUnario::NodoUnario(std::unique_ptr<Lexema> lexema, Precedencia precedencia, Asociatividad asociatividad)
: Nodo(std::move(lexema), precedencia, asociatividad) {

}

std::unique_ptr<numero_variante> NodoUnario::resolver() {
    std::unique_ptr<numero_variante> respuesta_derecha = nullptr;
    if (hijo_der)
        respuesta_derecha = hijo_der->resolver();
    if (!respuesta_derecha) { // si no se pudo resolver
        this->error = hijo_der->obtener_error();
        return nullptr;
    }

    if (std::holds_alternative<mpz_class>(*respuesta_derecha)) {    // si el apuntador respuesta contiene un entero.
        mpz_class resp_entero = std::get<mpz_class>(*respuesta_derecha);
        return hacer_operacion(&resp_entero);
    }
    // si no, contiene un número real
    mpfr::mpreal resp_real = std::get<mpfr::mpreal>(*respuesta_derecha);
    return hacer_operacion(&resp_real);
}

NodoParentesis::NodoParentesis(std::unique_ptr<Lexema> lexema)
: NodoUnario(std::move(lexema), Precedencia::NUMERO){

}

std::unique_ptr<numero_variante> NodoParentesis::hacer_operacion(const mpz_class *a) {
    return std::make_unique<numero_variante>(*a);   // el nodo paréntesis solo devuelve el valor.
}

std::unique_ptr<numero_variante> NodoParentesis::hacer_operacion(const mpfr::mpreal *a) {
    return std::make_unique<numero_variante>(*a);
}

NodoMenosUnario::NodoMenosUnario(std::unique_ptr<Lexema> lexema)
: NodoUnario(std::move(lexema), Precedencia::MENOS_UNARIO) {

}

std::unique_ptr<numero_variante> NodoMenosUnario::hacer_operacion(const mpz_class *a) {
    mpz_class negativo = mpz_class(-*a);
    return std::make_unique<numero_variante>(negativo);
}

std::unique_ptr<numero_variante> NodoMenosUnario::hacer_operacion(const mpfr::mpreal *a) {
    mpfr::mpreal negativo = mpfr::mpreal(-*a);
    return std::make_unique<numero_variante>(negativo);
}

NodoBinario::NodoBinario(std::unique_ptr<Lexema> lexema, Precedencia precedencia,
                         Asociatividad asociatividad)
                         : Nodo(std::move(lexema), precedencia, asociatividad) {

}

std::unique_ptr<numero_variante> NodoBinario::resolver() {
    std::unique_ptr<numero_variante> respuesta_izquierda = nullptr;
    std::unique_ptr<numero_variante> respuesta_derecha = nullptr;
    if (hijo_izq)
        respuesta_izquierda = hijo_izq->resolver();
    if (!respuesta_izquierda) {
        this->error = hijo_izq->obtener_error();
        return nullptr;
    }

    if (hijo_der)
        respuesta_derecha = hijo_der->resolver();
    if (!respuesta_derecha) {
        this->error = hijo_der->obtener_error();
        return nullptr;
    }

    // si ambas respuestas son enteras, pasar como argumentos.
    if (std::holds_alternative<mpz_class>(*respuesta_izquierda) &&
            std::holds_alternative<mpz_class>(*respuesta_derecha)) {
        mpz_class resp_entero_izq = std::get<mpz_class>(*respuesta_izquierda);
        mpz_class resp_entero_der = std::get<mpz_class>(*respuesta_derecha);
        return hacer_operacion(&resp_entero_izq, &resp_entero_der);
    }

    // si no, convertir a reales
    mpfr::mpreal resp_real_izq = convertir_a_real(respuesta_izquierda.get());
    mpfr::mpreal resp_real_der = convertir_a_real(respuesta_derecha.get());
    return hacer_operacion(&resp_real_izq, &resp_real_der);
}

NodoSuma::NodoSuma(std::unique_ptr<Lexema> lexema)
: NodoBinario(std::move(lexema), Precedencia::SUMA) {

}

std::unique_ptr<numero_variante> NodoSuma::hacer_operacion(const mpz_class *a, const mpz_class *b) {
    mpz_class suma = mpz_class(*a + *b);
    return std::make_unique<numero_variante>(suma);
}

std::unique_ptr<numero_variante> NodoSuma::hacer_operacion(const mpfr::mpreal *a, const mpfr::mpreal *b) {
    mpfr::mpreal suma = mpfr::mpreal(*a + *b);
    return std::make_unique<numero_variante>(suma);
}

NodoResta::NodoResta(std::unique_ptr<Lexema> lexema)
: NodoBinario(std::move(lexema), Precedencia::RESTA) {

}

std::unique_ptr<numero_variante> NodoResta::hacer_operacion(const mpz_class *a, const mpz_class *b) {
    mpz_class resta = mpz_class(*a - *b);
    return std::make_unique<numero_variante>(resta);
}

std::unique_ptr<numero_variante> NodoResta::hacer_operacion(const mpfr::mpreal *a, const mpfr::mpreal *b) {
    mpfr::mpreal resta = mpfr::mpreal(*a - *b);
    return std::make_unique<numero_variante>(resta);
}

NodoMultiplicacion::NodoMultiplicacion(std::unique_ptr<Lexema> lexema)
: NodoBinario(std::move(lexema), Precedencia::MULTIPLICACION) {

}

std::unique_ptr<numero_variante> NodoMultiplicacion::hacer_operacion(const mpz_class *a, const mpz_class *b) {
    mpz_class producto = mpz_class(*a * *b);
    return std::make_unique<numero_variante>(producto);
}

std::unique_ptr<numero_variante> NodoMultiplicacion::hacer_operacion(const mpfr::mpreal *a, const mpfr::mpreal *b) {
    mpfr::mpreal producto = mpfr::mpreal(*a * *b);
    return std::make_unique<numero_variante>(producto);
}

NodoDivision::NodoDivision(std::unique_ptr<Lexema> lexema)
        : NodoBinario(std::move(lexema), Precedencia::DIVISION) {

}

std::unique_ptr<numero_variante> NodoDivision::hacer_operacion(const mpz_class *a, const mpz_class *b) {
    mpfr::mpreal a_real = convertir_a_real(a);
    mpfr::mpreal b_real = convertir_a_real(b);    // para que la división sea real.
    return hacer_operacion(&a_real, &b_real);
}

std::unique_ptr<numero_variante> NodoDivision::hacer_operacion(const mpfr::mpreal *a, const mpfr::mpreal *b) {
    if (*b == 0) {
        this->error = TipoDeError::DIVISION_ENTRE_CERO;
        return nullptr;
    }

    mpfr::mpreal division = mpfr::mpreal(*a / *b);
    return std::make_unique<numero_variante>(division);
}

NodoPotencia::NodoPotencia(std::unique_ptr<Lexema> lexema)
        : NodoBinario(std::move(lexema), Precedencia::POTENCIA, Asociatividad::DERECHA) {

}

std::unique_ptr<numero_variante> NodoPotencia::hacer_operacion(const mpz_class *a, const mpz_class *b) {
    if (*b > std::numeric_limits<unsigned long>::max()) {    // si b no se puede convertir a entero sin signo
        mpfr::mpreal a_real = convertir_a_real(a);
        mpfr::mpreal b_real = convertir_a_real(b);
        return hacer_operacion(&a_real, &b_real);
    }

    if (*b >= 0) {   // si el exponente es positivo se realiza la operación directamente.
        unsigned long exponente = (*b).get_ui();
        mpz_class potencia;
        mpz_pow_ui(potencia.get_mpz_t(), (*a).get_mpz_t(), exponente);
        return std::make_unique<numero_variante>(potencia);
    } else {    // si no
        mpz_class negativo = -*b;    // se obtiene la parte positiva del exponente
        unsigned long exponente = negativo.get_ui();
        mpz_class potencia_denominador;
        mpz_pow_ui(potencia_denominador.get_mpz_t(), (*a).get_mpz_t(), exponente); // se realiza la potencia.
        mpfr::mpreal potencia;
        potencia = 1 / convertir_a_real(&potencia_denominador); // se aplica x^-a = 1/x^a
        return std::make_unique<numero_variante>(potencia);
    }

}

std::unique_ptr<numero_variante> NodoPotencia::hacer_operacion(const mpfr::mpreal *a, const mpfr::mpreal *b) {
    mpfr::mpreal potencia;
    mpfr_pow(potencia.mpfr_ptr(), (*a).mpfr_ptr(), (*b).mpfr_ptr(), MPFR_RNDN);
    return std::make_unique<numero_variante>(potencia);
}
