/* Derechos reservados 2021 Sergio Andrés
 *
 * This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <iostream>

#include "calculadora_chida_config.h"
#include "interprete.h"

class Calculadora {
public:
    Calculadora() = default;

    static void inic() {
        std::cout << "Calculadora chida, versión ";
        std::cout << calculadora_chida_VERSION_MAYOR << "." << calculadora_chida_VERSION_MENOR << std::endl;
    }

    int ejecutar() {
        inic();
        std::string res;

        std::cout << std::endl << "> ";
        while (std::getline(std::cin, expresion) && expresion != "salir") {
            res = resultado();
            std::cout << res;
            std::cout << std::endl << "> ";
        }
        return 0;
    }

private:
    std::string expresion;

    std::string resultado() {
        Interprete interpete = Interprete(expresion);
        return interpete.interpretar();
    }
};

int main()
{
    setlocale(LC_ALL, "");
    Calculadora calc;
    return calc.ejecutar();
}
