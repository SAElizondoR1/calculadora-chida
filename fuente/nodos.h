//
// Created by sergio on 29/03/21.
//

#ifndef CALCULADORA_CHIDA_NODOS_H
#define CALCULADORA_CHIDA_NODOS_H

#include <memory>
#include <utility>
#include <variant>

#include <gmpxx.h>
#include <mpfr.h>
#include <mpreal.h>

#include "analizador_lexico.h"
#include "errores.h"


typedef std::variant<mpz_class, mpfr::mpreal> numero_variante;

enum class Precedencia {
    DESCONOCIDO = -1,
    MAXIMA = 0,
    SUMA = 1,
    RESTA = 1,
    MULTIPLICACION = 2,
    DIVISION = 2,
    POTENCIA = 3,
    MENOS_UNARIO = 4,
    NUMERO = 5
};

enum class Asociatividad {
    IZQUIERDA,
    DERECHA
};

class Nodo {
public:
    Nodo(std::unique_ptr<Lexema> lexema, Precedencia precedencia,
         Asociatividad asociatividad=Asociatividad::IZQUIERDA);
    [[nodiscard]] bool comparar_precedencia(Nodo *nodo) const;
    // verdadero si el nodo tiene mayor precedencia que el argumento.
    virtual std::unique_ptr<numero_variante> resolver() { return nullptr; }
    // resuelve recursivamente la expresión representada por el nodo (e hijos).
    Lexema* obtener_lexema();    // obtener lexema como nodo común.
    TipoDeError obtener_error();

protected:
    std::unique_ptr<Lexema> lexema;
    Nodo* padre;
    std::unique_ptr<Nodo> hijo_izq;
    std::unique_ptr<Nodo> hijo_der;
    Precedencia precedencia;    // precedencia del símbolo que representa el nodo.
    Asociatividad asociatividad;
    TipoDeError error;  // error encontrado al resolver el nodo.

    [[nodiscard]] static mpfr::mpreal convertir_a_real(const mpz_class *ap_numero);   // argumento entero
    [[nodiscard]] static mpfr::mpreal convertir_a_real(const numero_variante *ap_numero);   // argumento entero o real

    friend class AnalizadorSintactico;
};

class NodoNumero : public Nodo {
public:
    explicit NodoNumero(std::unique_ptr<Lexema> lexema);
};

class NodoEntero : public NodoNumero {
public:
    explicit NodoEntero(std::unique_ptr<Lexema> lexema);
    std::unique_ptr<numero_variante> resolver() override;
};

class NodoReal : public NodoNumero {
public:
    explicit NodoReal(std::unique_ptr<Lexema> lexema);
    std::unique_ptr<numero_variante> resolver() override;
};

class NodoUnario: public Nodo {
public:
    NodoUnario(std::unique_ptr<Lexema> lexema, Precedencia precedencia,
               Asociatividad asociatividad=Asociatividad::DERECHA);
    std::unique_ptr<numero_variante> resolver() override;

private:
    virtual std::unique_ptr<numero_variante> hacer_operacion(const mpz_class *a) { return nullptr; }
    virtual std::unique_ptr<numero_variante> hacer_operacion(const mpfr::mpreal *a) { return nullptr; }
};

class NodoParentesis : public NodoUnario {
public:
    explicit NodoParentesis(std::unique_ptr<Lexema> lexema);

private:
    std::unique_ptr<numero_variante> hacer_operacion(const mpz_class *a) override;
    std::unique_ptr<numero_variante> hacer_operacion(const mpfr::mpreal *a) override;
};

class NodoMenosUnario : public NodoUnario {
public:
    explicit NodoMenosUnario(std::unique_ptr<Lexema> lexema);

private:
    std::unique_ptr<numero_variante> hacer_operacion(const mpz_class *a) override;
    std::unique_ptr<numero_variante> hacer_operacion(const mpfr::mpreal *a) override;
};

class NodoBinario: public Nodo {
public:
    NodoBinario(std::unique_ptr<Lexema> lexema, Precedencia precedencia,
                Asociatividad asociatividad=Asociatividad::IZQUIERDA);
    std::unique_ptr<numero_variante> resolver() override;

private:
    virtual std::unique_ptr<numero_variante> hacer_operacion(const mpz_class *a, const mpz_class *b) {
        return nullptr; }
    virtual std::unique_ptr<numero_variante> hacer_operacion(const mpfr::mpreal *a, const mpfr::mpreal *b) {
        return nullptr; }
    // el comportamiento de estos método se define el las subclases.
};

class NodoSuma : public NodoBinario {
public:
    explicit NodoSuma(std::unique_ptr<Lexema> lexema);

private:
    std::unique_ptr<numero_variante> hacer_operacion(const mpz_class *a, const mpz_class *b) override;
    std::unique_ptr<numero_variante> hacer_operacion(const mpfr::mpreal *a, const mpfr::mpreal *b) override;
};

class NodoResta : public NodoBinario {
public:
    explicit NodoResta(std::unique_ptr<Lexema> lexema);

private:
    std::unique_ptr<numero_variante> hacer_operacion(const mpz_class *a, const mpz_class *b) override;
    std::unique_ptr<numero_variante> hacer_operacion(const mpfr::mpreal *a, const mpfr::mpreal *b) override;
};

class NodoMultiplicacion : public NodoBinario {
public:
    explicit NodoMultiplicacion(std::unique_ptr<Lexema> lexema);

private:
    std::unique_ptr<numero_variante> hacer_operacion(const mpz_class *a, const mpz_class *b) override;
    std::unique_ptr<numero_variante> hacer_operacion(const mpfr::mpreal *a, const mpfr::mpreal *b) override;
};

class NodoDivision : public NodoBinario {
public:
    explicit NodoDivision(std::unique_ptr<Lexema> lexema);

private:
    std::unique_ptr<numero_variante> hacer_operacion(const mpz_class *a, const mpz_class *b) override;
    std::unique_ptr<numero_variante> hacer_operacion(const mpfr::mpreal *a, const mpfr::mpreal *b) override;
};

class NodoPotencia : public NodoBinario {
public:
    explicit NodoPotencia(std::unique_ptr<Lexema> lexema);

private:
    std::unique_ptr<numero_variante> hacer_operacion(const mpz_class *a, const mpz_class *b) override;
    std::unique_ptr<numero_variante> hacer_operacion(const mpfr::mpreal *a, const mpfr::mpreal *b) override;
};

#endif //CALCULADORA_CHIDA_NODOS_H
