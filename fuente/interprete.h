//
// Created by sergio on 29/03/21.
//

#ifndef CALCULADORA_CHIDA_INTERPRETE_H
#define CALCULADORA_CHIDA_INTERPRETE_H

#include <memory>
#include "analizador_sintactico.h"

class Interprete {
public:
    explicit Interprete(const std::string& texto);

    std::string interpretar();

private:
    std::unique_ptr<AnalizadorSintactico> analizador_sintactico;
};

#endif //CALCULADORA_CHIDA_INTERPRETE_H
