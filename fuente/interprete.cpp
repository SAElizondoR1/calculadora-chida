//
// Created by sergio on 29/03/21.
//

#include <sstream>
#include <gmpxx.h>

#include "interprete.h"

Interprete::Interprete(const std::string& texto) {
    analizador_sintactico = std::make_unique<AnalizadorSintactico>(texto);
}

std::string Interprete::interpretar() {
    std::unique_ptr<Nodo> raiz_arbol = analizador_sintactico->analizar();

    if (analizador_sintactico->obtener_error() != TipoDeError::NINGUNO)
        switch (analizador_sintactico->obtener_error()) {
            case TipoDeError::ERROR_LEXICO:
                return "Expresión inválida. No entendí el símbolo \"" +
                        raiz_arbol->obtener_lexema()->obtener_valor() + "\".";
            case TipoDeError::ERROR_SINTACTICO:
                return "Expresión inválida. Error de sintáxis.";
            default:
                return "Expresión inválida.";
        }

    std::unique_ptr<numero_variante> respuesta = raiz_arbol->resolver();

    if (raiz_arbol->obtener_error() != TipoDeError::NINGUNO)
        switch (raiz_arbol->obtener_error()) {
            case TipoDeError::DIVISION_ENTRE_CERO:
                return "Expresión inválida. La división entre 0 no está definida.";
            case TipoDeError::EXPONENTE_MAYOR_AL_LIMITE:
                return "No se pueden resolver operaciones con exponente mayor a 4294967295.";
            default:
                return "No se pudo resolver la expresión.";
        }

    if (std::holds_alternative<mpz_class>(*respuesta)) {
        mpz_class respuesta_entero = std::get<mpz_class>(*respuesta);
        return respuesta_entero.get_str();
    } else {
        mpfr::mpreal respuesta_real = std::get<mpfr::mpreal>(*respuesta);
        std::ostringstream respuesta_real_salida;
        respuesta_real_salida << respuesta_real;
        return respuesta_real_salida.str();
    }
}