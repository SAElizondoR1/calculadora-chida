//
// Created by sergio on 04/02/21.
//

#ifndef CALCULADORA_CHIDA_ANALIZADOR_SINTACTICO_H
#define CALCULADORA_CHIDA_ANALIZADOR_SINTACTICO_H

#include <string>
#include <memory>

#include "analizador_lexico.h"
#include "nodos.h"
#include "errores.h"

class AnalizadorSintactico {
public:
    explicit AnalizadorSintactico(const std::string &texto);
    std::unique_ptr<Nodo> analizar();
    TipoDeError obtener_error();

private:
    std::unique_ptr<AnalizadorLexico> analizador_lexico;
    std::unique_ptr<Lexema> lexema_actual;
    std::unique_ptr<Nodo> raiz; // Nodo raiz del arbol.
    Nodo* mas_ala_derecha; // Nodo mas a la derecha del arbol
    TipoDeError error;  // Error encontrado al crear árbol.

    bool crear_arbol(); // Devuelve verdadero si se creó satisfactoriamente, falso de lo contrario.
    bool consumir_expresion(TipoDeLexema hastaDonde); // Ej. (2-3)*4-6, devuelve si se pudo.
    bool consumir_argumento();  // Ej. (2-3) en (2-3)*4-6, devuelve si se pudo.
    bool consumir_operaciones(TipoDeLexema hastaDonde); // Ej. *4-6 en (2-3)*4-6, devuelve si se pudo.
    void insertar_nodo(std::unique_ptr<Nodo> nodo, bool esBinario); // Insertar nodo en árbol.
};

#endif //CALCULADORA_CHIDA_ANALIZADOR_SINTACTICO_H
