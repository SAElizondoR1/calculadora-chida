# Requisitos

Se recomienda instalar los requisitos desde los repositorios de su distribución.

* [CMake](https://cmake.org/download/) (mínimo versión 3.10)

* [Biblioteca de aritmética de precisión múltiple de GNU (gmp)](https://gmplib.org/manual/)

* [GMP con soporte para punto flotante (mpfr)](https://www.mpfr.org/mpfr-current/mpfr.html)

* [MPFR para C++ (mpfrc++)](http://www.holoborodko.com/pavel/mpfr/)

# Instrucciones de instalación

1. En la terminal, posiciónese en el directorio donde desee descargar el programa (`cd [nombre del directorio]`).

2. Descargue el programa en el directorio. Puede utilizar el siguiente comando:

`git clone https://gitlab.com/SAElizondoR1/calculadora-chida.git`

3. Cambie al directorio base del programa:

`cd calculadora-chida`

3. Ejecute el programa de instalación con el siguiente comando (se necesitan permisos de superusuario):

`sudo bash instalar.sh`

4. Ahora puede abrir el programa con el comando `cchida`.

# Instrucciones de desinstalación

1. Posiciónese en el directorio donde descargó el programa (`cd [nombre del directorio]`).

2. Cambie al directorio base del programa:

`cd calculadora-chida`

3. Ejecute el programa de desinstalación con el siguiente comando (se necesitan permisos de superusuario):

`sudo bash desinstalar.sh`

# Sintáxis de la calculadora

**Suma**: +

**Resta**: -

**Multiplicación**: *

**División**: /

**Potencia**: ^

**Paréntesis**: ()

**Menos unario**: -

**Más unario**: +

**Salir**: escriba "salir" o presione Ctrl + C
